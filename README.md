# Netwit

## Cloning this project
```git clone --recurse-submodules --depth 1 https://gitlab.com/ura2j/netwit.git --recursive```

## Installation 
To run this you will need to install hugo **Extended Version!!**

To do this on linux do ```snap install hugo --channel=extended```
## Running the website locally
install dependencies (last two commands may require you to be a super user, on linux add sudo before the next two commands)
```
npm install
npm install -D --save autoprefixer
npm install -D --save postcss-cli
```
Then run the project
```
hugo server
```
You can access this in your browser at: 
```http://localhost:1313/netwit/```