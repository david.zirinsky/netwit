--- 
title: "Renting"
linkTitle: "Currently Renting"
type: docs
weight: 20
Description: >
  Information for individuals having trouble with rented property.
--- 
### Table of contents

1. [Repairs and Maintenance](#tenant-and-landlord-mantainance-requirements)
    - [What to do if your landlord will not fix an issue](#what-if-my-landlord-will-not-fix-the-issue)
2. [Rent Payment Information](#what-does-my-landlord-have-to-show-me) 

## Tenant and landlord mantainance requirements 

#### Under Virginia law all landlords must do these things (unless you both legally agreed to something different):

- Follow building and housing codes affecting health and safety.
 						
- Make all repairs needed to keep the place fit and habitable.
 						
- Keep in good and safe working order all electrical, plumbing, sanitary, heating, ventilating, air conditioning and other facilities and appliances that the landlord supplies.
 						
- Prevent or remove rodent infestations. ([If you are having bed bug issues click here for more information](https://ura2j.gitlab.io/netwit/tenant/rights/currently-renting/bedbugsinrentalhousing/))
 						
- Maintain clean and safe common areas used by more than one tenant.

### If something needs to be repaired that is the landlord’s responsibility, but you must first:

**1. Notify the landlord in writing of the problem.** 

**2. Give the landlord a reasonable time to fix it.** 

#### What is the timeframe for my landlord to fix the issue? 

In _emergency_ situations (like lack of heat or water) your landlord must fix it _immediately_. Immediately = within hours, or up two days. 

_Other repairs_ must be fixed within a _reasonable time_. Reasonable time = about 1-2 weeks. 

#### What should I include in the letter to my landlord? 

1. The repairs needed

2. The time by which to fix each problem 
    
    - Choose either _immediately_ or _reasonable time_ for each repair 

3. What times of day are best for you to be home to let the landlord in to fix the issue

    - If you can't be home, provide contact information so the landlord can reach you for permission to enter your home
    
4. Note: Get a certified mail return receipt when you send this letter (good for record-keeping)

## What if my landlord WILL NOT fix the issue? 

You can take your landlord to court to make them fix it or have the issue fixed otherwise. 

Note: 
At this point, it is best to get legal help. If you do get a lawyer, tell the lawyer you may have a _rent escrow_ case.

#### What steps should I take to start the rent escrow process? 
To use this procedure, you pay your full rent into court within five days of the date the rent first comes due. You fill out a "Tenant’s Assertion and Complaint" form at the General District Court for the county or city where you live. You can attach a copy of the inspection report or your repair letter to the landlord.

You also can list the bad conditions on the form. 

#### Basics of a rent escrow case? 

What you do (if on your own):

1. Pay your full rent to your local General District Court within 5 days from when your rent is due. 

2. Fill out a _Tenant’s Assertion and Complaint_ Form at the General District Court.  
    - [Click here for forms and filing information](https://ura2j.gitlab.io/netwit/tenant/docs/)
 
3. List the bad conditions on the form.

4. With the form, submit a copy of either 1. the inspection report detailing your issue (your building inspector should have given you this when the issue was checked out) OR 2. your repair letter to the landlord (only 2. here if you paid an outside repairman to repair the issue instead of the landlord).

#### How much will it cost to file the form? 

About $30. 

This includes: 1. the filing with the court fee and 2. the serving to your landlord fee.

You may ask the clerk for _waiver of fee_ if you can’t afford to pay (but you will have to prove you cannot pay). 

#### What should I include on the tenants assertion form? 

Choose an option from below of what you want the judge to do:

1. to order repairs completed before your rent is released to the landlord

2. to order repairs and return of some (or all) of the rent money to you for having to put up with the bad conditions

3. to order your lease ended so you can move out without paying future rent	

#### What happens once I file a tenants assertion with the general district Court?

The court sets a hearing date and has the landlord served with a _summons to appear in court_. 

You can also ask the clerk to summon (with a _subpoena_) the building inspector (if there was one) and any other witnesses who have agreed to help you. 

##### How much does a summons cost? 

$12 each, unless your filing fees are waived. 

### How should I prepare for the hearing? 

1. A list of repairs needed

2. A copy of your lease if written

3. A copy of your notice letter

4. Your certified mail return receipt from when you mailed your notice letter to the landlord

5. Your building inspector’s report of the repairs needed

6. Any pictures or videos related to the repair needed

7. Your rent receipts

#### What will happen when the judge calls my case? 

**Note: If you do not come to court on your trial date, the court will dismiss your case (lost opportunity and wasted your time).** 

However, if you come to court and the landlord does not, you'll probably get a favorable judgment. 

If you and your landlord come to court, the judge will hear both of you then decide who wins.  	

When the case is heard, you will present your evidence first (the stuff you put together in the question before this one). The landlord or judge may ask you questions - answer them as best you can and as calmly as you can. Ask the inspector and any of your witnesses to testify after you. 

Then the landlord gets to present evidence and witnesses. You can question the witnesses about what they have said, **but do not argue with them.** (_Good Example_: Isn't it true that you saw the hole in my ceiling? _Bad Example_: I know you saw the hole in my ceiling. Don't pretend it wasn't bad.)

---- 

## What does my landlord have to show me? 

Your landlord is required to give you a written receipt whenever the tenant pays rent in the form of cash or a money order as long as you ask the landlord for a receipt. 

This is true by law, even if it’s not stated in a written lease. You should ALWAYS request a receipt every time you pay rent by cash or a money order.
 							
If you want an accounting of all the charges from the landlord and the payments you’ve made, you should make a written request to the landlord. The landlord is required to give you a written statement showing all the charges and payments over the entire time you have been a tenant with the building (up to 12 months). The landlord must give you this within 10 business days after receiving your request. 

## [Information and Forms for Filing With the Court](https://ura2j.gitlab.io/netwit/tenant/docs/)
