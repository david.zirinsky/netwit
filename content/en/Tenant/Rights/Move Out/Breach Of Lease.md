--- 
title: "Breach of Lease"
linkTitle: "Breach of Lease"
type: docs
toc_hide: true
weight: 50
--- 

## What Is a "Breach of Lease"? 

A breach of lease is when a tenant or landlord breaks one of the agreements in the lease.


If your breach/problem is not non-payment of rent, the landlord can send you a notice saying if you don’t fix the problem within 21 days, the lease will terminate in 30 days.			

### What If I Fixed the Problem?  

Even if you fix the problem, if the same problem happens again, the landlord does not have to give you another 21 days to fix it. The landlord can simply give you a 30-day notice to end the lease. If the problem cannot be fixed, the landlord can give you a 30-day notice right away.

### What If I haven't Paid Rent? 

If you fail to pay the rent or other charges on time, the landlord can try to end your lease by sending you a 5-day pay or quit notice. If you pay all the money requested within 5 days your lease will not be ended.			

### When Does My Landlord Not Have to Give Me Notice? 

If you commit a criminal or willful act that is a threat to health or safety, the landlord is not required to give you notice, but rather can simply go to court and end your lease.


