---
title: "Evictions (including Lockouts and Utility Shutoffs)"
linkTitle: "Evictions"
type: docs
toc_hide: true
weight: 50
---

## Evictions (including lockouts and utility shutoffs)

Every tenant has the legal right to live in rental housing unless and until the landlord follows the legal process for eviction. You are a tenant if you pay regular amounts of rent during regular time periods, such as once a month or once a week. You also are a tenant if you have lived in a hotel or motel for more than 90 days, or you are subject to a written lease for a period of more than 90 days. You are not a tenant if you have lived in a hotel or motel for less than 90 days. In this case, the only legal right you have is to receive a five-day “pay or quit” notice before your landlord evicts you by self-help without going to court.

### What type of notice does a landlord have to give to evict?

No matter what the reason, a landlord must give you a written notice in order to evict. However, you do not have to move just because a landlord has given written notice.

### Is a landlord’s oral notice to move any good?

No. A landlord’s oral notice to move is not good. An oral notice to move should not allow the landlord to start an eviction. You do not have to move just because a landlord has given an oral notice.

### What type of notice does a landlord have to give in a non-payment of rent case?

If a landlord wants to evict you for not paying rent, the landlord must give you a written notice to either move or pay rent in 5 days. For tenants in conventional public housing, the landlord must give you a written notice to either move or pay rent in 14 days. If you pay the rent within the time period, you get to stay. If you do not pay, the landlord can start an unlawful detainer (eviction) lawsuit in general district court (GDC). You do not have to move just because the landlord has filed a lawsuit.

### What type of notice does a landlord have to give in other cases?

If the landlord wants to evict for a reason other than non-payment of rent, the landlord must give you a written notice can be sent stating that if the problem is not corrected within 21 days, the lease will terminate in 30-days. This notice must explain the problem or reason the landlord wants to evict. If you correct the problem in 21 days, you get to stay. If not, the landlord can start an unlawful detainer in GDC after the 30-day period.

Even if you correct the problem, if the same problem happens again, the landlord does not have to give you another 21 day time period to fix the problem, but rather can simply serve you with a 30-day notice. In addition, if the breach of the lease is not capable of being corrected, then a straight 30-day notice can be set. If you do not move by the end of the 30 days, the landlord may start an unlawful detainer in GDC after the 30-day period.

### What type of notice can a landlord if there is a threat to health or safety?

If you commit a criminal or willful act that is a threat to health or safety, the landlord is not required to send a notice to terminate your tenancy, but rather can simply proceed with the filing of a court action for possession.

## What are the steps in an unlawful detainer (eviction) lawsuit?

A landlord must follow these steps in an unlawful detainer (eviction) lawsuit.

 File a lawsuit in court. The lawsuit may be filed either in general district court or in circuit court

### Circuit court. Almost all evictions are filed in general district court.

Serve (legally deliver) you a copy of the court papers in a manner allowed by law.

 Go to court at the date and time of your hearing.

 Get a judgment of possession from the court.

 Get a Writ of Eviction from the court. This is the paper that allows the Sheriff to evict

you.

## How does a landlord file an unlawful detainer?

A landlord starts an unlawful detainer in general district court by filing a Summons for Unlawful Detainer. Although this court paper is called an “unlawful” detainer, it is not used in a criminal case. It is used only in a civil (non-criminal) case.

### How does a landlord serve an unlawful detainer?

An unlawful detainer must be served (legally delivered) on you. This may be done in three different ways.

    1. Given to you in person, usually by a Deputy Sheriff or a private process server.

    2. Given to a member of your household, usually by a Deputy Sheriff or private process server. The household member must be 16 or older. The person serving the unlawful detainer must explain what it is.

    3. Posted on your front door and then mailed to you by first class mail.

An unlawful detainer can be legally served on you, even if you never actually get it. If it was properly given to a household member who didn’t tell you about it, you still were legally served. If an unlawful detainer was properly posted and mailed to you but you never saw it, you still were legally served. Both these things are unusual, but they do happen. You should tell household members to pay attention to court papers, and you should pay attention to yourself.

## What do unlawful detainer papers say?

These papers tell you the date, time, and place of your court hearing. The papers also tell you the amount of money the landlord is claiming, such as rent, interest, late fees, damages, court costs, and attorney’s fees. The hearing may be your only chance to dispute or oppose the eviction and the claim for money. In all eviction cases, go to the hearing. Get there early so you can find your courtroom and watch how the court handles other cases.

## What if I can’t go to my unlawful detainer hearing?

If you can’t go to general district court on the date of your unlawful detainer hearing,

you must ask the court for a new hearing date. This is called a “continuance.” Different general district courts have different rules for getting a continuance. In some courts, the Clerk can give a continuance. In other courts, only the Judge can give a continuance.

To find out the rule for your court, call the Clerk’s Office as soon as you know you can’t go to court on the date of your court hearing. Ask to be told the rule to get a continuance, and follow that rule. In addition to calling the Clerk’s Office, it’s a good idea to write and fax a letter to the court explaining why you need a continuance.

## What should I do at an unlawful detainer hearing?

If you go to general district court to dispute or oppose the eviction, get prepared for your hearing in advance. Bring papers, receipts and witnesses that support your case. If a witness doesn’t want to come to court, you can ask the Clerk to subpoena the witness.

A subpoena is a court order that says a witness must come to court. You must pay $12.00 for the subpoena, and you must ask for it at least 10 days before your hearing date. If you don’t have enough money to pay this (or any other) fee, ask the Clerk for the “Petition for Proceeding in Civil Case Without Payment of Fees or Costs.” This also is called “Form CC-1414.”

## Do I need a lawyer in general district court?

You don’t need a lawyer in general district court, but a lawyer can help you. You may have defenses to the eviction.

## What defense could I have in a non-payment of rent case?

If the only reason the landlord has for evicting is non-payment of rent, you may stay in the rental unit if you pay everything owed on or before the court date. This is called a redemption (pay and stay), or a redemption tender (an offer to pay and stay).

A redemption means the eviction lawsuit must be dismissed as paid if you pay the landlord, the landlord’s attorney, or the court all amounts owed as of the court date. All amounts owed means all rent (including a new month’s rent if that has come due), all late fees set forth in a written lease (including a new month’s late fee if that has come due, court costs, and reasonable attorney’s fees (if a landlord’s attorney is involved). If there is a redemption, always get a receipt and come to court with the receipt to make sure the case is dismissed paid.

A redemption tender means you come to court on the first court date and show the judge a written commitment from a local government or non-profit agency to pay all or part of the redemption amount. If so, the judge must postpone the case ten days and allow you to come back with the full redemption amount on that day. Again, get receipts and come to court with them to be sure the case is dismissed as paid. If not, the landlord gets an order of possession.

As of July 1, 2019, tenants will get a fourth chance to pay their rent late and stay, which is an extended right of redemption (extended right to pay and stay). If the landlord wins the lawsuit, the judge will issue an order of possession. After that, the landlord may ask the court to issue a writ of eviction. This goes from the clerk to the Sheriff to the tenant, and authorizes the Sheriff to evict on a specific date. The Sheriff must give you at least 72 hours advance notice of the eviction, and usually gives about 7-10 days.

Under the extended right of redemption, you can pay the landlord, the landlord’s attorney, or the court all amounts owed as of two business days before the Sheriff’s scheduled eviction date. All amounts owed means all rent (including a new month’s rent if that has come due), all late fees set forth in a written lease (including a new month’s late fee if that has come due), court costs, Sheriff’s fees, and reasonable attorney’s fees (if a landlord’s attorney is involved). Payment must be by cashier’s check, certified check, or money order. If so, the Sheriff’s eviction is canceled. Confirm with both the landlord and the Sheriff to be sure.

You may do a redemption, a redemption tender, or an extended redemption, only once in any 12 month period of time that you continue to live in the same place.

## What other defense could I have?

One defense is that the landlord did not keep the place in good shape. To use this defense, you must be current in rent. In addition, the landlord must have been told about the problem in writing – either by you or by someone or your behalf – before the unlawful detainer was filed. You also must pay rent to court instead of the landlord.

Another defense is that the landlord wants to evict because you complained or used legal rights. To use this defense, the landlord must know that you complained to the landlord or government agency about a rental housing problem, or that you joined a tenant’s group, before the unlawful detainer is filed.

## Do I have to go to the unlawful detainer hearing?

If you don’t want to oppose the eviction, you don’t have to go to court. You won’t be arrested if you do not go to court. That only happens in criminal cases. This is a civil (non-criminal) case. If you don’t go to court, and the other side does and proves its case, you will lose the eviction case.

## What happens in court?

At the court date, the judge will call your name and ask whether you admit or deny what the landlord said in the unlawful detainer. If you admit it, the Court will enter an order of possession for the property, as well as a money judgment for the rent, damages, costs, and fees sought by the landlord. If the landlord asks, the Judge can give immediate possession and allow the landlord to get a Writ of Eviction right away. However, you cannot be evicted until your 10 day appeal period has passed. If the landlord does not ask for immediate possession, the Writ cannot be issued until the 10 day appeal period has passed. The Sheriff will provide you the Writ of Eviction, as well as at least 72 hours’ notice of the date the actual eviction will occur.

If you disagree with the landlord, the court will usually set another date to actually try the case. The judge also will ask both you and the landlord if you want the other side to put in writing why each feels they are right. The landlord’s writing is called a Bill of Particulars which explains why they are entitled to both possession and any money sought. Your writing is called a Grounds of Defense which explains why the landlord is wrong. The judge will set dates when these are due to be filed with the court and to be sent to the other side. If they are not done by the required dates, you can automatically lose without ever having the trial

If you ask the case to be set for another day, the landlord can ask the court that you pay all the rent owed to the court until the trial date. Unless you have a good defense to the case, the judge will give you 7 days to pay the money to the court. If you fail to make the payment on time, the landlord can ask for judgment without ever having the trial.

When the case is heard, all witnesses will have to speak under oath. The landlord goes first. You will get a chance to question the landlord and any other witnesses. After that, you and your witnesses will testify. Then the landlord gets to ask questions. In the end, each side can make a short closing argument, telling all the reasons the judge should decide the case for the landlord or the tenant. After hearing both sides, the judge will decide the case. If you win, you can stay on as if the case never came to court. The losing party has a 10 day appeal period.

## How do I appeal if I lose an eviction lawsuit?

To appeal from general district court to circuit court, you must do three things within ten days of the judgment:

- Fill out a Notice of Appeal (Form DC-475) in the general district court clerk’s office

- Pay, or get the court to waive (forgive), the fees of the circuit court.

- Pay, or get the court to waive (forgive), the appeal bond.

In a nonpayment of rent case, the appeal bond cannot be waived. Starting July 1, 2019, to appeal an eviction judgment based on nonpayment of rent, you still must post an appeal bond for the amount of the money judgment for rent. But after that, you must only pay ongoing rent as it becomes due.

### Can a landlord lock out a tenant or shut off utilities?

No! The landlord may not shut off utilities, lock you out of the rental unit, or evict you without giving notice and going to court. You do not have to move out just because the landlord tells you to leave and takes out an unlawful detainer. The landlord must wait until a court order is issued. These steps usually take more than 2 months from the day you get a notice to move out.

If a landlord cuts off utilities, locks you out of the rental unit, or evicts you without giving notice and going to court, you have a quick remedy. Go to general district court and file a Tenant’s Petition for Relief from Unlawful Exclusion (Form DC-431). You can file this on your own, by yourself, without an attorney.

To file and serve the papers will cost about $58. You may ask the clerk for a “Petition for Proceeding in Civil Case Without Payment of Fees or Costs” (Form CC-1414) if you can’t afford to pay. When you fill out the Tenant’s Petition, you fill in the name and physical address of the true owner of the property as the Defendant-Landlord.

If you are not completely certain about the name and physical address of the true owner of the property, you will have to do a real property search. Google “real property search” and the name of your County or Independent City. For example, “Richmond real property search.” You also can call your local officials in your County or Independent City.

If the true owner is not a natural person – for example, a corporation or a limited liability company (LLC) – there is one more thing you have to do. You must get the name and physical address of the registered agent of the company. To get this, call the Virginia State Corporation Commission at 804-371-9733 or 866-722-2551. When you fill out the Tenant’s Petition, you fill in the name of the company as the Defendant-Landlord, and the name and physical address of their registered agent.

When you fill out the Tenant’s Petition, you need to decide what you want the judge to do. You can ask the judge for any of these things: allow you back into possession, resume the cut off utility, end the rental agreement, and get actual damages and attorney’s fees.

After filing the Tenant’s Petition, the court sets a hearing date and has the landlord served with a summons to court. You can also ask the clerk to subpoena the building inspector if there was one, and any other witnesses who have agreed to help you. Subpoenas cost $12 each unless your filing fees were waived. Before the hearing date, you should get together your evidence.

When the case is heard, you will present your evidence first. The landlord or judge may ask you questions. Then the landlord gets to present evidence and witnesses. You can question them about what they have said, but don’t argue with them. If you do not come to court on your trial date, the court will dismiss your case. If you come to court and the other side does not, you should get a judgment. If both sides come to court, the judge will hear both sides and decide who wins.


