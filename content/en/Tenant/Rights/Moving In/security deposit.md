--- 
title: "Security Deposits"
linkTitle: "Security deposit"
type: docs 
toc_hide: true
weight: 20
--- 
## Virginia Security Deposit Information 

Most landlords will make you pay a security deposit before you move in. By law the deposit cannot be more than two months' rent. The deposit is held by the landlord until you move out, in case there are any damages to the home, unpaid rent, or other charges you owe. If you didn't damage the home or owe any money, you will get your security deposit back. Otherwise, the landlord gets to keep the security deposit.	

When you move out, the landlord can use part of the security deposit to cover any unpaid utility bills you are supposed to pay under the lease. But the landlord must first give you written notice at least 15 days before using the deposit. After paying the utility bills, the landlord must give you written confirmation of payment within 10 days, and also give you the rest of the security deposit. On the other hand, if you can provide written proof that you actually paid the utility bills, the landlord must refund the security deposit.

The landlord must return the deposit or send you an itemized list of the damages or charges deducted from the deposit within 45 days of when you move out. Also if you have lived there for more than 13 months, the landlord must give you interest on the deposit.

If you are not covered under the VRLTA (See “Legal Information” tab for more information), there is no interest or specific time limit for the return of the deposit. However, if it is not returned after a reasonable amount of time, you can go to court and sue for its return.

### IMPORTANT TIPS:			

 - Always thoroughly check the rooms, appliances, and plumbing before you move in.
 						
 - As soon as you move in, make a list of all the things wrong, give a copy to the landlord and keep a copy for yourself. You may also wish to take pictures before you move in, so you have a record of the condition when you moved in. This will protect you from being charged for existing damages.
 						
 - When you are ready to move out, make an appointment with the landlord to inspect the premises together so you can agree on its condition.
 						
 - If you are concerned about getting the deposit back, you may also want to take pictures when you move out so you can later prove how you left the premises.
 						
 - Always return the keys and if you expect return of the deposit, leave a forwarding address.
 						
 - When you move, make sure to move everything out as quickly as possible. Things you leave behind for too long can be treated as abandoned.
 						
 - Unless specifically agreed to by the landlord, do not assume the security deposit covers your last month’s rent because your landlord could evict you if don’t pay the rent on time.   		
