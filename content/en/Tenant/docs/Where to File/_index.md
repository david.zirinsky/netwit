---
title: "Where to File"
linkTitle: "Where to File"
weight: 3
date: 2017-01-05
type: docs
description: >
  Information for where to file a Tenants Assertion Form in Virginia
---

## Where Should I File The Form 

Now that you have a completed Tenant’s Assertion and Complaint form, you can take it to the court house. You even have the address-- it is written on the top of the form! When you get to the court house, go to the clerk of the court and tell him or her that you wish to file a Tenant’s Assertion. The clerk will ask for the form and may also ask for the return receipt from the landlord letter.

- Note: You must file the form at the local district where you live. For a map of a Map of Virginia's Judicial Circuits and Districts [Click Here](http://www.courts.state.va.us/courts/maps/home.html). For case status and case infomration [click here](https://eapps.courts.state.va.us/gdcourts/captchaVerification.do?landing=landing)

## What Happens Next

The clerk will then establish something called an “escrow account” for you. This is just a place where you can pay your rent to the judge so that you can be current on your rent while your landlord is fixing the problems. remember—you must be current on your rent, and you are required to pay the judge your rent within 5 days of the due date in your lease if your rent comes due again before the problems are fixed. 

The Court will set a “Return Date” for you and the landlord to appear at court and argue your case, present evidence such as the pictures you took, and take account of damages. This hearing should occur within 15 days of but you can ask for an earlier date if it is an emergency.

