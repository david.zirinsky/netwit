---
title: "Navigation AI"
linkTitle: "Navigation AI"
type: docs
toc_hide: false
weight: 90
Description: >
  An example of how easy it is to add an AI tool to assist in guiding users to the correct page
---


### Use the tool below to have AI navigate you to the correct page or see some of the other available tools and resources below
<iframe class="guideclearly-viewer" src="https://guideclearly.com/viewer/guide/247" width="315" height="420"></iframe>
