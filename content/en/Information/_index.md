---
title: "Information"
weight: 1
type: docs
toc_hide: true
description: >
  A special section with a docs layout.
---

{{% pageinfo %}}

## Tools 

[Write Your Landlord a Letter and Fill Out A Form](https://lawhelpinteractive.org/Interview/GenerateInterview/7163/engine)

[Videos](https://netwit.gitlab.io/hugo/va_info/ta-videos/)

[Speak to the Community](/comunity)

{{% /pageinfo %}}


This section is conatins all of the information that you need to fill out and file a Tenants Assertion form with the court.  

For large documentation sets we recommend adding content under the headings in this section, though if some or all of them don’t apply to your project feel free to remove them or add your own. You can see an example of a smaller Docsy documentation site in the [Docsy User Guide](https://docsy.dev/docs/), which lives in the [Docsy theme repo](https://github.com/google/docsy/tree/master/userguide) if you'd like to copy its docs section. 

Other content such as marketing material, case studies, and community updates should live in the [About](/about/) and [Community](/community/) pages.

Find out how to use the Docsy theme in the [Docsy User Guide](https://docsy.dev/docs/). You can learn more about how to organize your documentation (and how we organized this site) in [Organizing Your Content](https://docsy.dev/docs/best-practices/organizing-content/).
