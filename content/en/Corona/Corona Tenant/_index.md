--- 
title: Tenant Coronavirus Information
link title: Tenant Coronavirus Information
type: docs
weight: 10
Description: >
  Information Regarding Evictions for Current Residents Of Virginia Affected By the Corona Virus
--- 
## Your landlord must take you to court to evict you:

- Your landlord cannot evict you without a court order, no matter what your lease says. 
- You do not have to move out just because the landlord tells you to leave, gives you a “5 Day Pay or Quit” or other written notice, or files an eviction lawsuit (“unlawful detainer”) against you.


## Court closures:
 
 - Courts have stopped hearing unlawful detainer cases until least April 6, 2020. 
 - We do not know when the courts will schedule new eviction cases.
 - Still, if you have an unlawful detainer case scheduled for the next few weeks, you should check with the court to find out when it will be heard. You can also lookup your case and find the court’s phone number by visiting http://www.courts.state.va.us/courts/gd.html.
 - You can still go to the court for emergencies, like if your landlord illegally locks you out. 

## It is illegal for your landlord to shut off utilities or lock you out without a court order:
- If the landlord locks you out or cuts off heat, water, gas, or electricity without a court order, you can call your local sheriff or police for help.
- You can also sue your landlord to get back into your home and get utilities turned back on. The courts are still hearing emergency cases like these, called “unlawful exclusions.”

## Your landlord still has a duty to do repairs and maintenance:
- Even with the COVID-19 outbreak, the landlord must still provide you with a safe and healthy place to live.
- If something breaks, send your landlord a letter asking for repairs right away.

## Your utilities, including water and electricity, should not be cut off:
- The State Corporate Commission has told electric, gas, and water companies not to cut service for non-payment until the COVID-19 crisis ends.
- If your Dominion Energy service was recently cut off, call 1-866-366-4357 to get reconnected.

## If you are facing eviction, if your landlord attempts to lock you out without taking you to court, or if you have questions about your rights, contact your local legal aid by calling 1-866-LEGL-AID or get legal advice from the Eviction Legal Helpline by calling 1-833-NoEvict. Visit VaLegalAid.org for more information. 
