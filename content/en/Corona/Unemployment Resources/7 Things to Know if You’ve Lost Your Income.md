--- 
title: 7 Things to Know if You’ve Lost Your Income 
link title: Unemployment Resources
type: docs
weight: 20
--- 

This is an uncertain and stressful time for all of us, but especially for those of us who can’t afford to pay our bills. Below, seven things to know if you’ve recently lost your income. 

## 1.	You have at least two potential sources of financial help available to you: 
 - Expanded unemployment benefits that cover not only regular employees whose hours have 
been reduced or cut completely, but also those who are self-employed and independent contractors. You can apply online at http://www.vec.virginia.gov/. If you are denied benefits, you can seek free legal assistance from your local legal aid office by calling 1-866-LEGLAID. 
- The CARE Act government checks of $1,200 per person (plus $500 for each dependent child) will be issued within a few weeks. Not everyone will be getting these checks, and some people will need to take action to get the check.  

## 2.	Try to avoid high-interest loans. 
Don’t be tempted to take a high-interest loan while you're waiting for your benefits and your CARE Act checks from the government. This may end up making your situation much worse. 

## 3. Courts are closed. That means no evictions and no debt collection lawsuits... for now. 
The courts in Virginia are effectively shut down until at least April 26. That means no evictions, no debt collection lawsuits ("warrants in debt"), and no garnishments are going to be issued by the court. Even if you can't pay your rent right now, your landlord can't take you to court to evict you—and it's illegal to try to put you out without going to court. It’s a good idea to let your landlord know your situation. 

## 4. Utilities can’t be cut off. 
The State Corporation Commission has ordered the various utilities it regulates not to terminate services to customers who can't pay their utility bills during the crisis. 

## 5. Foreclosures have been halted. 
The federal government has ordered a moratorium on foreclosures on federally insured mortgages until May 17. Contact your mortgage holder and a nonprofit housing counselor if you cannot make your payments. 

## 6. Limit access to your bank account. 
You may have given authorization to some of your creditors to automatically debit your bank account for payments. Put a stop to this so that you can get control over which creditors are paid first. The CFPB has instructions here. You can use your bank’s bill pay service to make automatic payments to creditors—this gives you control over starting and stopping those payments. 

## 7. If you have debt, pay it off in the following order (as a general rule): 

  ### 1.	Rent or house payments 
  ### 2.	Outstanding utility bills 
  ### 3.	Food and other basic necessities 
  ### 4.	Car payments 
  ### 5.	Any other remaining debt 

If you're facing financial trouble and potential eviction; foreclosure; garnishment; a debt collection lawsuit; or are denied unemployment benefits, Medicaid, food stamps, or other public benefits: 
•	Call your local legal aid office at 1-866-LEGLAID. 
•	Visit: https://www.nclc.org/for-consumers/guide-to-surviving-debt.html for more information 
on surviving debt. 
•	Visit www.valegalaid.org for legal information for people struggling financially and to locate the legal aid office that serves your area in Virginia. 
